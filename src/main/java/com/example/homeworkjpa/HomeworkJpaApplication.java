package com.example.homeworkjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeworkJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomeworkJpaApplication.class, args);
    }

}
