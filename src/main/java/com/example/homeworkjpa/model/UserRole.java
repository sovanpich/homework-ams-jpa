package com.example.homeworkjpa.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.apache.catalina.User;

import javax.persistence.*;

@Entity
@Setter
@Getter
public class UserRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


//    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "users-fk")
//    private Users users;
////
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "role_fk")
    @JsonManagedReference
    private Role role;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_fk")
    @JsonManagedReference
    private Users users;
}
