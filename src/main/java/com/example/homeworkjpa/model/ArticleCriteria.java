package com.example.homeworkjpa.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleCriteria {
    private String category;
    private String user;
}
