package com.example.homeworkjpa.controller;

import com.example.homeworkjpa.model.Article;
import com.example.homeworkjpa.model.Users;
import com.example.homeworkjpa.payload.Request.UserDTO;
import com.example.homeworkjpa.response.ResponseHeader;
import com.example.homeworkjpa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserRestController {
    @Autowired
    private UserService userService;

    @GetMapping("/findAll")
    public ResponseEntity<Object> findAll(){

        try {
            List<Users> usersList = userService.findAll();
            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"), usersList);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<Object> add(@RequestBody UserDTO userDTO){
        try {
            Users users = userService.add(userDTO);
            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"), users);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable Long id, @RequestBody UserDTO userDTO){
        try {
            Users users = userService.update(id, userDTO);
            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"), users);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }

    }
    @GetMapping("/findAllWithPage/{page}/{size}")
    public ResponseEntity<Object> findAllPaging (@PathVariable int page,@PathVariable int size){
        try {
            Page<Users> users = userService.findALlWithPage(page,size);
            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"),users);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }

    @GetMapping("/findByRole/{role}")
    public List<Users> findByRole(@PathVariable String role){
        return userService.findByRole(role);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id){
        userService.delete(id);
    }
}
