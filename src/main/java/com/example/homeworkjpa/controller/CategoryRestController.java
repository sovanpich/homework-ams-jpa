package com.example.homeworkjpa.controller;

import com.example.homeworkjpa.model.Article;
import com.example.homeworkjpa.model.Category;
import com.example.homeworkjpa.payload.Request.CategoryDTO;
import com.example.homeworkjpa.response.ResponseHeader;
import com.example.homeworkjpa.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryRestController {
    @Autowired
    private CategoryService categoryService;
    @GetMapping("/findAll")
    public ResponseEntity<Object> findAll(){
        try {
            List<Category> categoryList = categoryService.findAllCategory();
            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"),categoryList);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }
    @PostMapping("/add")
    public ResponseEntity<Object> add(@RequestBody CategoryDTO categoryDTO){
        try {
            Category category = categoryService.addCategory(categoryDTO);
            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"),category);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> update(@PathVariable Long id,@RequestBody CategoryDTO category){
        try {
            Category category1 = categoryService.update(id,category);
            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"),category);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }
    @PutMapping("/findAllWithPage/{page}/{size}")
    public ResponseEntity<Object> findAllPaging (@PathVariable int page,@PathVariable int size){
        try {
            Page<Category> categories = categoryService.categoryPaging(page,size);
            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"),categories);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id){
        categoryService.delete(id);
    }

}
