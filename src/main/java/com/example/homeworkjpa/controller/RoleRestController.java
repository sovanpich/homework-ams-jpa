package com.example.homeworkjpa.controller;

import com.example.homeworkjpa.model.Role;
import com.example.homeworkjpa.payload.Request.RoleDTO;
import com.example.homeworkjpa.response.ResponseHeader;
import com.example.homeworkjpa.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleRestController {
    @Autowired
    private RoleService roleService;

    @GetMapping("/findAll")
    public ResponseEntity<Object> findAll(){

        try {
            List<Role> roleList = roleService.findAll();
            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"),roleList);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<Object> add(@RequestBody RoleDTO roleDTO){

        try {
            Role role = roleService.add(roleDTO);
            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"),role);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> update(@PathVariable Long id, @RequestBody RoleDTO roleDTO){
        try {
            Role role = roleService.update(id,roleDTO);
            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"),role);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }

    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id){
        roleService.delete(id);
    }
}
