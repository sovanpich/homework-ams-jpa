package com.example.homeworkjpa.controller;

import com.example.homeworkjpa.model.Article;
import com.example.homeworkjpa.payload.Request.ArticleDTO;
import com.example.homeworkjpa.repository.ArticleRepository;
import com.example.homeworkjpa.response.ResponseHeader;
import com.example.homeworkjpa.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/article")
public class ArticleRestController {
    @Autowired
    private ArticleService articleService;
    @Autowired
    private ArticleRepository articleRepository;

    @PostMapping("/add")
    public ResponseEntity<Object> add(@RequestBody ArticleDTO articleDTO){
        try {
            Article article = articleService.add(articleDTO);
            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"),article);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }

    @GetMapping("/findAll")
    public ResponseEntity<Object> findAll(){
        try {
            List<Article> articleList = articleService.findAll();

            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"),articleList);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }
//@GetMapping("/findAll")
//public List<Article> findAll(){
//    return articleService.findAll();
//}

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id){
        articleService.delete(id);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> update(@PathVariable Long id,@RequestBody ArticleDTO articleDTO){
        try {
            Article article = articleService.update(id,articleDTO);
            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"),article);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }

    @GetMapping("/findByUser/{User_id}")
    public List<Article> findByUser(@PathVariable("User_id") Long id){
        return articleService.findByUser(id);
    }

    @GetMapping("/findByCategory/{CategoryId}")
    public List<Article> findByCategory(@PathVariable("CategoryId") Long id){
        return articleService.findByCategory(id);
    }
    @GetMapping("/findBy/{user}/{category}")
    public List<Article> findByUC (@PathVariable String user,@PathVariable String category){
        return articleService.findByUserAndCate(user,category);
    }

    @GetMapping("/findAllPaging/{page}/{size}")
    public ResponseEntity<Object> findAllPaging (@PathVariable int page,@PathVariable int size){
        try {
            Page<Article> article = articleService.ArticlePaging(page,size);
            return ResponseHeader.generateResponse("Successful!", HttpStatus.valueOf("OK"),article);
        }catch (Exception e) {
            return ResponseHeader.generateResponse(e.getMessage(),HttpStatus.MULTI_STATUS,null);
        }
    }
}
