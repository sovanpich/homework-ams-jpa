package com.example.homeworkjpa.payload.Request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ArticleDTO {
    private String title;
    private String description;
    private String category;
    private Long user_id;
    private Long category_id;
}
