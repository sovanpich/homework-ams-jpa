package com.example.homeworkjpa.repository;

import com.example.homeworkjpa.model.Article;
import com.example.homeworkjpa.model.Category;
import com.example.homeworkjpa.payload.Request.ArticleDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
public interface ArticleRepository extends JpaRepository<Article,Long>, PagingAndSortingRepository<Article,Long> , JpaSpecificationExecutor<Article> {

    Article findArticleById(Long id);

    ArticleDTO getArticleByUsers(Long id);

    @Query(value = "SELECT a.* from article a INNER join users u On a.create_by=u.id WHERE u.id =:id",nativeQuery = true)
    List<Article> findAricleByUserId(Long id);

    @Query(value = "SELECT a.* from article a INNER join category c On a.category_id=c.id WHERE c.id =:id",nativeQuery = true)
    List<Article> findArticleByCategoryId(Long id);

//    Page <Article> findAllWithPaging (Pageable pageable);
}
