package com.example.homeworkjpa.repository;

import com.example.homeworkjpa.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<Users,Long>, PagingAndSortingRepository<Users,Long> {

    Users findUserById (Long id);

    @Query(value ="SELECT u.* FROM users u INNER JOIN user_role ur INNER JOIN role r ON ur.role_fk = r.id ON u.id=ur.user_fk WHERE lower(r.name) like lower(concat('%', :role,'%'))",nativeQuery = true)
    List<Users> findByRole(String role);

    @Query(value = "DELETE  FROM users where id=:id",nativeQuery = true)
    void deleteUser (Long id);


}
