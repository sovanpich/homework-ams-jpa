package com.example.homeworkjpa.repository;

import com.example.homeworkjpa.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRoleRepository extends JpaRepository<UserRole,Long> {
    List<UserRole> findUserRoleByRole(Long id);

}
