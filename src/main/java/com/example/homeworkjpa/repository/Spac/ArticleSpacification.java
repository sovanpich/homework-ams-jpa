package com.example.homeworkjpa.repository.Spac;

import com.example.homeworkjpa.model.Article;
import com.example.homeworkjpa.model.ArticleCriteria;
import com.example.homeworkjpa.model.Category;
import com.example.homeworkjpa.model.Users;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class ArticleSpacification implements Specification<Article> {
    @Autowired
    private ArticleCriteria articleCriteria;

    @Override
    public Predicate toPredicate(Root<Article> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        if(articleCriteria.getCategory()==null || articleCriteria.getUser()==null) return null;
        else {
            Join<Article,Category>  joinCategory =root.join("categories",JoinType.INNER);
            Join<Article, Users>  joinUser = root.join("users",JoinType.INNER);
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(joinCategory.get("name")),articleCriteria.getCategory()+"%"));
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(joinUser.get("username")),articleCriteria.getCategory()+"%"));
        }
        Predicate[] pre = new Predicate[predicates.size()];
        return criteriaBuilder.or(predicates.toArray(pre));
    }
}
