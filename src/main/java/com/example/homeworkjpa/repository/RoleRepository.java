package com.example.homeworkjpa.repository;

import com.example.homeworkjpa.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role,Long> {

    Role findRoleById(Long id);
}
