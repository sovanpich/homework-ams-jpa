package com.example.homeworkjpa.repository;

import com.example.homeworkjpa.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category,Long>, PagingAndSortingRepository<Category,Long> {
    Category findCategoryById(Long id);

}
