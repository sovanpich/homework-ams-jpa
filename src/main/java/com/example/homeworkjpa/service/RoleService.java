package com.example.homeworkjpa.service;

import com.example.homeworkjpa.model.Role;
import com.example.homeworkjpa.payload.Request.RoleDTO;


import java.util.List;

public interface RoleService {
    Role add(RoleDTO roleDTO);

    List<Role> findAll();

    Role findById(Long id);

    Role update(Long id,RoleDTO roleDTO);

    void   delete(Long id);
}
