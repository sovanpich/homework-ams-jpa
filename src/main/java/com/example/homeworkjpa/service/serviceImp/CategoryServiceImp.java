package com.example.homeworkjpa.service.serviceImp;

import com.example.homeworkjpa.model.Category;
import com.example.homeworkjpa.payload.Request.CategoryDTO;
import com.example.homeworkjpa.repository.CategoryRepository;
import com.example.homeworkjpa.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Category addCategory(CategoryDTO categoryDTO) {
        Category category = new Category();
        category.setName(categoryDTO.getName());
        return categoryRepository.save(category);
    }

    @Override
    public List<Category> findAllCategory() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findById(Long id) {
        return categoryRepository.findCategoryById(id);
    }

    @Override
    public Category update(Long id, CategoryDTO categoryDTO) {
        Category categoryUpdate = categoryRepository.findCategoryById(id);
        categoryUpdate.setId(id);
        categoryUpdate.setName(categoryDTO.getName());
        return categoryRepository.save(categoryUpdate);
    }

    @Override
    public void delete(Long id) {
        Category category = categoryRepository.findCategoryById(id);

        categoryRepository.delete(category);

    }

    @Override
    public Page<Category> categoryPaging(int page, int size) {
        Sort sort = Sort.by(Sort.Direction.ASC,"name");
        Pageable pageable = PageRequest.of(page,size,sort);
        return categoryRepository.findAll(pageable);
    }
}
