package com.example.homeworkjpa.service.serviceImp;

import com.example.homeworkjpa.model.Article;
import com.example.homeworkjpa.model.ArticleCriteria;
import com.example.homeworkjpa.model.Category;
import com.example.homeworkjpa.model.Users;
import com.example.homeworkjpa.pagination.Paging;
import com.example.homeworkjpa.payload.Request.ArticleDTO;
import com.example.homeworkjpa.repository.ArticleRepository;
import com.example.homeworkjpa.repository.CategoryRepository;
import com.example.homeworkjpa.repository.Spac.ArticleSpacification;
import com.example.homeworkjpa.repository.UserRepository;
import com.example.homeworkjpa.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService {
    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public Article add(ArticleDTO articleDTO) {

        Category category = categoryRepository.findCategoryById(articleDTO.getCategory_id());
        Users users = userRepository.findUserById(articleDTO.getUser_id());
        Article article = new Article();
        article.setTitle(articleDTO.getTitle());
        article.setDescription(articleDTO.getDescription());
        article.setCategories(category);
        article.setUsers(users);
        return  articleRepository.save(article);
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        Article article = articleRepository.findArticleById(id);
        articleRepository.delete(article);
    }

    @Override
    public Article update(Long id, ArticleDTO articleDTO) {
        Category category = categoryRepository.findCategoryById(articleDTO.getCategory_id());
        Users users = userRepository.findUserById(articleDTO.getUser_id());
        Article article1 = articleRepository.findArticleById(id);
        article1.setTitle(articleDTO.getTitle());
        article1.setDescription(articleDTO.getDescription());
        article1.setUsers(users);
        article1.setCategories(category);

        return articleRepository.save(article1);
    }

    @Override
    public List<Article> findByUserAndCate(String user, String category) {
        ArticleCriteria articleCriteria = new ArticleCriteria();
        articleCriteria.setUser(user);
        articleCriteria.setCategory(category);
        ArticleSpacification articleSpacification = new ArticleSpacification(articleCriteria);
        return articleRepository.findAll(articleSpacification);
    }
    @Override
    public Page<Article> ArticlePaging(int page,int size) {
        Sort sort = Sort.by(Sort.Direction.ASC,"title");
        Pageable pageable = PageRequest.of(page,size,sort);
        return articleRepository.findAll(pageable);
    }


//    @Override
//    public Page<Article> paging(Pageable pageable) {
//        return articleRepository.findAllWithPaging(pageable);
//    }


    @Override
    public List<Article> findByCategory(Long id) {
        return articleRepository.findArticleByCategoryId(id);
    }

    @Override
    public List<Article> findByUser(Long id) {
        return articleRepository.findAricleByUserId(id);
    }
}
