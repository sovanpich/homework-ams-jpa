package com.example.homeworkjpa.service.serviceImp;

import com.example.homeworkjpa.model.Role;
import com.example.homeworkjpa.payload.Request.RoleDTO;
import com.example.homeworkjpa.repository.RoleRepository;
import com.example.homeworkjpa.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImp implements RoleService {
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role add(RoleDTO roleDTO) {
        Role role = new Role();
        role.setName(roleDTO.getName());
        return roleRepository.save(role);
    }

    @Override
    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    @Override
    public Role findById(Long id) {
        return roleRepository.findRoleById(id);
    }

    @Override
    public Role update(Long id, RoleDTO roleDTO) {
        Role role = roleRepository.findRoleById(id);
        role.setName(roleDTO.getName());
        return roleRepository.save(role);
    }

    @Override
    public void delete(Long id) {
        Role role = roleRepository.findRoleById(id);
        roleRepository.delete(role);
    }
}
