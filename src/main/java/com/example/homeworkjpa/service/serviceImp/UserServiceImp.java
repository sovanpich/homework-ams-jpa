package com.example.homeworkjpa.service.serviceImp;

import com.example.homeworkjpa.model.Role;
import com.example.homeworkjpa.model.UserRole;
import com.example.homeworkjpa.model.Users;
import com.example.homeworkjpa.payload.Request.UserDTO;
import com.example.homeworkjpa.repository.RoleRepository;
import com.example.homeworkjpa.repository.UserRepository;
import com.example.homeworkjpa.repository.UserRoleRepository;
import com.example.homeworkjpa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;

    @Override
    public Users add(UserDTO userDTO) {
        Role role = roleRepository.findRoleById(userDTO.getRole_id());


        Users user = new Users();
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());

        UserRole userRole = new UserRole();
        userRole.setRole(role);
        userRole.setUsers(user);
        userRoleRepository.save(userRole);

        return userRepository.save(user);
    }

    @Override
    public List<Users> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Users findById(Long id) {
        return userRepository.findUserById(id);
    }

    @Override
    public Users update(Long id, UserDTO userDTO) {
        Role role1 = roleRepository.findRoleById(userDTO.getRole_id());
        UserRole userRole1 = userRoleRepository.getById(userDTO.getRole_id());

        Users users = userRepository.findUserById(id);
        users.setId(id);
        users.setUsername(userDTO.getUsername());
        users.setPassword(userDTO.getPassword());

        UserRole userRole = new UserRole();
        userRole.setRole(role1);
        userRole.setUsers(users);
        userRoleRepository.save(userRole);

        return userRepository.save(users);
    }

    @Override
    public Page<Users> findALlWithPage(int page, int size) {
        Sort sort = Sort.by(Sort.Direction.ASC,"username");
        Pageable pageable = PageRequest.of(page,size,sort);
        return userRepository.findAll(pageable);
    }

    @Override
    public void delete(Long id) {
        Users users = userRepository.findUserById(id);
        userRepository.delete(users);
    }

    @Override
    public List<Users> findByRole(String role) {
        return userRepository.findByRole(role);
    }
}
