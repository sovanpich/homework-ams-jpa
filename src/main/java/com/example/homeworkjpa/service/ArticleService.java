package com.example.homeworkjpa.service;

import com.example.homeworkjpa.model.Article;
import com.example.homeworkjpa.model.Category;
import com.example.homeworkjpa.pagination.Paging;
import com.example.homeworkjpa.payload.Request.ArticleDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ArticleService {

    Article add(ArticleDTO articleDTO);

    List<Article> findAll();

    void delete(Long id);

    Article update(Long id, ArticleDTO articleDTO);

    Page<Article> ArticlePaging(int page,int size );

    List<Article> findByUser (Long id);

    List<Article> findByCategory (Long id);

    List<Article> findByUserAndCate (String user,String category);

//    Page <Article> paging (Pageable pageable);
}
