package com.example.homeworkjpa.service;

import com.example.homeworkjpa.model.Category;
import com.example.homeworkjpa.payload.Request.CategoryDTO;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CategoryService {
    Category addCategory(CategoryDTO categoryDTO);

    List<Category> findAllCategory();

    Category findById(Long id);

    Category update(Long id,CategoryDTO categoryDTO);

    void   delete(Long id);

    Page<Category> categoryPaging(int page, int size);
}
