package com.example.homeworkjpa.service;

import com.example.homeworkjpa.model.Users;
import com.example.homeworkjpa.payload.Request.UserDTO;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {
    Users add(UserDTO userDTO);

    List<Users> findAll();

    Users findById(Long id);

    Users update(Long id, UserDTO userDTO);

    void   delete(Long id);

    List<Users> findByRole (String role);

    Page<Users> findALlWithPage (int page,int size);
}

